<?php

require_once '../vendor/autoload.php';

\JustParallels\Bootstrap::init(new \JustParallels\Configuration());

$workers = \JustParallels\Bootstrap::getInstance()->getModelsManager()->getWorker()->getWorkers();
foreach ($workers as $worker) {
    $runner = new \JustParallels\Tests\Migrations\Runner($worker['id']);
    $runner->execute();
}
