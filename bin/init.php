<?php
/**
 * Created by PhpStorm.
 * User: vmelnychuk
 * Date: 10/1/16
 * Time: 12:31 AM
 */
require_once __DIR__ . '/../vendor/autoload.php';

\JustParallels\Bootstrap::init(new \JustParallels\Configuration());

