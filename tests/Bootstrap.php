<?php

require_once '../vendor/autoload.php';
$bootstrap = new \JustParallels\Bootstrap();
$bootstrap->setConfiguration(new \JustParallels\Configuration());
$bootstrap->setTaskManager(new \JustParallels\Tasks\TaskManager());
$bootstrap->setModelsManager(new \JustParallels\Model\ModelManager());
\JustParallels\Bootstrap::setInstance($bootstrap);
