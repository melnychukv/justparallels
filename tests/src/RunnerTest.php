<?php

/**
 * Created by PhpStorm.
 * User: vmelnychuk
 * Date: 10/14/16
 * Time: 1:18 PM
 */
class RunnerTest extends \JustParallels\Tests\TestCase
{


    /**
     * @dataProvider getStacks
     * @param $stacks
     * @throws \JustParallels\Exception
     */
    public function testExecute_testDown($stacks)
    {
        $workerData          = new StdClass();
        $workerData->from    = 10;
        $workerData->to      = 20;
        $workerData->version = 1;
        $workerModel         = $this->getMockBuilder('JustParallels\Model\Worker')
            ->getMock();
        $workerModel->method('getData')->willReturn($workerData);
        $stackModel = $this->getMockBuilder('JustParallels\Model\Stack')
            ->getMock();
        $stackModel->method('getStacks')->willReturn($stacks);

        \JustParallels\Bootstrap::getInstance()->getModelsManager()->setStack($stackModel);
        \JustParallels\Bootstrap::getInstance()->getModelsManager()->setWorker($workerModel);
        $runner = new \JustParallels\Tests\Migrations\Runner(1);
        $runner->execute();

        /**
         * @var $output \JustParallels\Log\MigrationInfo[]
         */
        $output = \JustParallels\Tests\StackOutPut::getInstance()->getOutPut();


        $this->assertInstanceOf('\JustParallels\Log\MigrationInfo', $output[1]);
        $this->assertEquals(10, $output[1]->getStackVersion());
        $this->assertEquals(2, $output[1]->getStackId());
        $this->assertEquals(1, $output[1]->getMigrationVersion());

        $this->assertInstanceOf('\JustParallels\Log\MigrationInfo', $output[4]);
        $this->assertEquals(9, $output[4]->getStackVersion());
        $this->assertEquals(2, $output[4]->getStackId());
        $this->assertEquals(1, $output[4]->getMigrationVersion());

        $this->assertInstanceOf('\JustParallels\Log\MigrationInfo', $output[7]);
        $this->assertEquals(8, $output[7]->getStackVersion());
        $this->assertEquals(2, $output[7]->getStackId());
        $this->assertEquals(1, $output[7]->getMigrationVersion());

    }


    public function testExecute_AlreadyVersion()
    {
        $workerData          = new StdClass();
        $workerData->from    = 10;
        $workerData->to      = 20;
        $workerData->version = 8;
        $workerModel         = $this->getMockBuilder('JustParallels\Model\Worker')
            ->getMock();
        $workerModel->method('getData')->willReturn($workerData);
        $stackModel = $this->getMockBuilder('JustParallels\Model\Stack')
            ->getMock();
        $stacks     = array(
            array(
                'stack_id' => 1,
                'version'  => 8
            )
        );
        $stackModel->method('getStacks')->willReturn($stacks);

        \JustParallels\Bootstrap::getInstance()->getModelsManager()->setStack($stackModel);
        \JustParallels\Bootstrap::getInstance()->getModelsManager()->setWorker($workerModel);
        $runner = new \JustParallels\Tests\Migrations\Runner(1);

        $runner->execute();
        $output = \JustParallels\Tests\StackOutPut::getInstance()->getOutPut();

        /**
         * @var \JustParallels\Log\Message $message
         */
        $message = $output[0];
        $this->assertInstanceOf('JustParallels\Log\Message', $message);
        $this->assertEquals(8,$message->getMigrationInfo()->getMigrationVersion());
        $this->assertEquals(\JustParallels\Log\Message::TYPE__INFO, $message->getType());
        $this->assertEquals(\JustParallels\Log\Message::EVENT__MIGRATION_EXISTS, $message->getEvent());
        $this->assertEquals(1, $message->getMigrationInfo()->getStackId());
    }

    public function getStacks()
    {
        return array(
            array(
                array(
                    array(
                        'stack_id' => 1,
                        'version'  => 5
                    ),
                    array(
                        'stack_id' => 2,
                        'version'  => 10
                    ),
                    array(
                        'stack_id' => 3,
                        'version'  => 8
                    )
                )
            )
        );
    }
}
