<?php

/**
 * Created by PhpStorm.
 * User: vmelnychuk
 * Date: 10/1/16
 * Time: 12:40 AM
 */
class MigrationTest extends \JustParallels\Tests\TestCase
{

    public function testRun()
    {
        /**
         * @var \JustParallels\Model\Worker $workerModel
         */
        $workerModel = $this->getMockBuilder('JustParallels\Model\Worker')
            ->getMock();
        $workerModel->method('create')->willReturn(true);
        /**
         * @var \JustParallels\Model\Stack $stackModel
         */
        $stackModel = $this->getMockBuilder('JustParallels\Model\Stack')
            ->getMock();
        $stackModel->method('getCount')->willReturn(100);
        \JustParallels\Bootstrap::getInstance()->getModelsManager()->setWorker($workerModel);
        \JustParallels\Bootstrap::getInstance()->getModelsManager()->setStack($stackModel);
        $task = new \JustParallels\Tasks\CreateWorkers(1);
        $task->create(9);
        $this->assertEquals(array(2, 10, 20), $task->getParams(1, 10));
    }

}