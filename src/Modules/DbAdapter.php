<?php

/**
 * Created by PhpStorm.
 * User: vmelnychuk
 * Date: 9/30/16
 * Time: 11:25 PM
 */
namespace JustParallels\Modules;

use JustParallels\Configuration;
use Zend\Db\Adapter\Adapter;

class DbAdapter
{
    private static $instance;
    /**
     * @var Configuration
     */
    private $configuration;


    /**
     * DbAdapter constructor.
     * @param Configuration $configuration
     */
    public function __construct(Configuration $configuration)
    {

        $this->configuration = $configuration;
    }

    public function build()
    {
        return new Adapter($this->getConfig($this->configuration));
    }


    public static function getInstance()
    {
        return self::$instance;
    }

    protected function getConfig(Configuration $configuration)
    {

        return array(
            'driver'   => 'Mysqli',
            'database' => $configuration->db,
            'username' => $configuration->user,
            'password' => $configuration->password,
            'platform' => 'Mysql'
        );
    }
}