<?php
/**
 * Created by PhpStorm.
 * User: vmelnychuk
 * Date: 10/16/16
 * Time: 11:46 PM
 */

namespace JustParallels\Tests;


class StackOutPut
{
    /**
     * @var StackOutPut
     */
    private static $_instance;

    /**
     * @var array
     */
    private $_outPut = array();


    /**
     * @return StackOutPut
     */
    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * @param mixed $instance
     */
    public static function setInstance($instance)
    {
        self::$_instance = $instance;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        return $this->_outPut[$id];
    }

    /**
     * @return array
     */
    public function getOutPut()
    {
        return $this->_outPut;
    }

    /**
     * @param $element
     * @return $this
     */
    public function add($element)
    {
        $this->_outPut[] = $element;
        return $this;
    }


}