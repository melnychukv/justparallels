<?php
/**
 * Created by PhpStorm.
 * User: vmelnychuk
 * Date: 10/3/16
 * Time: 12:25 AM
 */

namespace JustParallels\Tests;


class TestCase extends \PHPUnit\Framework\TestCase
{
    protected function setUp()
    {
        parent::setUp();
    }

    protected function tearDown()
    {
        parent::tearDown();
        StackOutPut::setInstance(null);
    }


}