<?php
/**
 * Created by PhpStorm.
 * User: vmelnychuk
 * Date: 10/9/16
 * Time: 5:02 PM
 */

namespace JustParallels\Tests\Migrations;

use JustParallels\Log\Message;
use JustParallels\Log\MigrationInfo;
use JustParallels\Tests\StackOutPut;

use JustParallels\Migration;

class migrateLocations extends Migration
{
    public function getVersion()
    {
        return 8;
    }


    public function up(MigrationInfo $migrationInfo)
    {
        StackOutPut::getInstance()->add($migrationInfo);
    }

    public function down(MigrationInfo $migrationInfo)
    {
        StackOutPut::getInstance()->add($migrationInfo);
    }

    public function log(Message $message)
    {
        StackOutPut::getInstance()->add($message);
    }
}