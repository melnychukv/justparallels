<?php
/**
 * Created by PhpStorm.
 * User: vmelnychuk
 * Date: 10/9/16
 * Time: 5:16 PM
 */

namespace JustParallels\Tests\Migrations;


class Runner extends \JustParallels\Runner
{
    protected function getMigrations()
    {
        return array(
            new migrateUsers(),
            new migrateGroups(),
            new migrateLocations()
        );
    }


}