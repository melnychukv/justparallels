<?php
/**
 * Created by PhpStorm.
 * User: vmelnychuk
 * Date: 9/25/16
 * Time: 10:38 PM
 */

namespace JustParallels;


use JustParallels\Log\Message;
use JustParallels\Log\MigrationInfo;

abstract class Runner
{
    private $_migrations = array();
    private $_maxVersion = 0;
    private $_worker;

    /**
     * Runner constructor.
     * @param $workerId
     */
    public function __construct($workerId)
    {
        $this->_worker     = Bootstrap::getInstance()->getModelsManager()->getWorker()->getData($workerId);
        $this->_migrations = $this->_sortMigrations($this->getMigrations());
    }

    /**
     * @return array
     */
    abstract protected function getMigrations();

    public function execute()
    {
        $stacks = Bootstrap::getInstance()->getModelsManager()->getStack()
            ->getStacks($this->_worker->from, $this->_worker->to);

        Bootstrap::getInstance()->getModelsManager()->getWorker()->startWork($this->_worker->id);

        foreach ($stacks as $stack) {
            $version = $stack['version'];
            if ($version < $this->_worker->version) {

                $this->_migrateUp($stack['version'], $stack['stack_id']);
            } else if ($version > $this->_worker->version) {
                $this->_migrateDown($stack['version'], $stack['stack_id']);
            } else {
                /** @var Migration $migration */
                $migration     = $this->_migrations[$version];
                $migrationInfo = new MigrationInfo($stack['stack_id'], $stack['version'], $this->_worker->version);
                $migration->log(new Message($migrationInfo, Message::TYPE__INFO, 'Migration already exists',
                    Message::EVENT__MIGRATION_EXISTS));
            }
        }

        Bootstrap::getInstance()->getModelsManager()->getWorker()->endWork($this->_worker->id);
    }

    /**
     * @return int
     */
    public function getMaxVersion()
    {
        return $this->_maxVersion;
    }

    /**
     * @param int $maxVersion
     */
    public function setMaxVersion($maxVersion)
    {
        $this->_maxVersion = $maxVersion;
    }


    protected function _sortMigrations($migrations)
    {
        $resultMigration = array();
        /** @var Migration $migration */
        foreach ($migrations as $migration) {
            $version                   = $migration->getVersion();
            $resultMigration[$version] = $migration;

            if ($version > $this->getMaxVersion()) {
                $this->setMaxVersion($version);
            }
        }

        return $resultMigration;
    }


    /**
     * @param $stackVersion
     * @param $stackId
     */
    protected function _migrateUp($stackVersion, $stackId)
    {
        //when account has got version migration 8 when first started migration v9
        $stackVersion++;
        while ($stackVersion <= $this->_worker->version) {

            if (isset($this->_migrations[$stackVersion])) {
                $migrationInfo = new MigrationInfo($stackId, $stackVersion, $this->_worker->version);
                $migrationInfo->setStackVersion($stackVersion);
                /** @var Migration $migration */
                $migration = $this->_migrations[$stackVersion];
                $migration->log(new Message($migrationInfo, Message::TYPE__INFO, 'Migration started',
                    Message::EVENT__MIGRATION_START));
                try {
                    $migration->up($migrationInfo);
                } catch (\Exception $e) {
                    $migration->log(new Message($migrationInfo, Message::TYPE__ERROR, $e->getMessage()));
                }
                Bootstrap::getInstance()->getModelsManager()->getStack()->setVersion($stackVersion, $migrationInfo->getStackId());
                $migration->log(new Message($migrationInfo, Message::TYPE__INFO, 'Migration end',
                    Message::EVENT__MIGRATION_END));
            }
            $stackVersion++;
        }
    }


    /**
     * @param $stackVersion
     * @param $stackId
     * @throws Exception
     */
    protected function _migrateDown($stackVersion, $stackId)
    {
        //@todo check errors
        while ($stackVersion != $this->_worker->version) {

            if (isset($this->_migrations[$stackVersion])) {

                $migrationInfo = new MigrationInfo($stackId, $stackVersion, $this->_worker->version);
                /** @var Migration $migration */
                $migration = $this->_migrations[$stackVersion];
                $migration->log(new Message($migrationInfo, Message::TYPE__INFO, 'Migration started',
                    Message::EVENT__MIGRATION_START));
                try {
                    $migration->down($migrationInfo);
                } catch (\Exception $e) {
                    $migration->log(new Message($migrationInfo, Message::TYPE__ERROR, $e->getMessage()));
                }
                //after run migration 9 down. Account has got 8 migration version
                Bootstrap::getInstance()->getModelsManager()->getStack()->setVersion($stackVersion - 1,
                    $migrationInfo->getStackId());

                $migration->log(new Message($migrationInfo, Message::TYPE__INFO, 'Migration end',
                    Message::EVENT__MIGRATION_END));
            }
            $stackVersion--;
        }
    }

}