<?php
/**
 * Created by PhpStorm.
 * User: vmelnychuk
 * Date: 9/30/16
 * Time: 10:57 PM
 */

namespace JustParallels;


class Configuration
{

    public $db = 'justParallels';

    public $host = 'localhost';

    public $user = 'root';

    public $password;

    /**
     * @var DefaultConfigurations
     */
    public $default;

    /**
     * Configuration constructor.
     */
    public function __construct()
    {
        $this->default = new DefaultConfigurations();
    }
}

class DefaultConfigurations {

    public $parallels = 5;

    public $process = 5;
}