<?php
/**
 * Created by PhpStorm.
 * User: vmelnychuk
 * Date: 10/9/16
 * Time: 3:26 PM
 */

namespace JustParallels\Model;

class ModelManager
{
    private $_worker;
    private $_stack;


    /**
     * Manager constructor.
     */
    public function __construct()
    {
        $this->_worker = new Worker();
        $this->_stack = new Stack();
    }

    /**
     * @return Worker
     */
    public function getWorker()
    {
        return $this->_worker;
    }

    /**
     * @param Worker $worker
     */
    public function setWorker($worker)
    {
        $this->_worker = $worker;
    }

    /**
     * @return Stack
     */
    public function getStack()
    {
        return $this->_stack;
    }

    /**
     * @param Stack $stack
     */
    public function setStack($stack)
    {
        $this->_stack = $stack;
    }

}