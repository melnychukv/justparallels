<?php
/**
 * Created by PhpStorm.
 * User: vmelnychuk
 * Date: 10/1/16
 * Time: 2:53 AM
 */

namespace JustParallels\Model;


use JustParallels\Bootstrap;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Update;

class Worker
{
    const STATE_STACK       = 1;
    const STATE_IN_PROGRESS = 2;
    const STATE_COMPLETE    = 3;
    const STATE_FAILED      = 4;

    /**
     * @param $columns
     * @param $values
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     * @throws \JustParallels\Exception
     */
    public function create($columns, $values)
    {
        $sql = Bootstrap::getInstance()->getSql()->insert('worker')->columns($columns)->values($values);

        return Bootstrap::getInstance()->getSql()->prepareStatementForSqlObject($sql)->execute();
    }

    /**
     * @param $workerId
     * @return array|\ArrayObject|null
     * @throws \JustParallels\Exception
     */
    public function getData($workerId)
    {
        $select = Bootstrap::getInstance()->getSql()
            ->select('worker')
            ->where(array('id' => $workerId));

        $selectString = Bootstrap::getInstance()->getSql()->buildSqlString($select);
        return Bootstrap::getInstance()->getDbAdapter()
            ->query($selectString, Adapter::QUERY_MODE_EXECUTE)
            ->current();
    }

    /**
     * @param int $limit
     * @return array
     * @throws \JustParallels\Exception
     */
    public function getWorkers($limit = 5)
    {
        $select = Bootstrap::getInstance()->getSql()
            ->select('worker')
            ->where(array('state' => self::STATE_STACK));
        if($limit){
            $select->limit($limit);
        }

        $selectString = Bootstrap::getInstance()->getSql()->buildSqlString($select);
        return Bootstrap::getInstance()->getDbAdapter()
            ->query($selectString, Adapter::QUERY_MODE_EXECUTE)
            ->toArray();
    }

    /**
     * @param $workerId
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     * @throws \JustParallels\Exception
     */
    public function startWork($workerId)
    {
        $updatedAt = new \DateTime();
        $update    = new Update('worker');
        $update->set(array('state' => self::STATE_IN_PROGRESS, 'updated_at' => $updatedAt->format('Y-m-d H:i:s')));
        $update->where(array('id' => $workerId));

        return Bootstrap::getInstance()->getSql()->prepareStatementForSqlObject($update)->execute();
    }

    /**
     * @param $workerId
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     * @throws \JustParallels\Exception
     */
    public function endWork($workerId)
    {
        $updatedAt = new \DateTime();
        $update    = new Update('worker');
        $update->set(array('state' => self::STATE_COMPLETE, 'updated_at' => $updatedAt->format('Y-m-d H:i:s')));
        $update->where(array('id' => $workerId));

        return Bootstrap::getInstance()->getSql()->prepareStatementForSqlObject($update)->execute();
    }
}