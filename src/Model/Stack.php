<?php
/**
 * Created by PhpStorm.
 * User: vmelnychuk
 * Date: 10/1/16
 * Time: 2:53 AM
 */

namespace JustParallels\Model;


use JustParallels\Bootstrap;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Update;
use Zend\Db\Sql\Where;

class Stack
{

    const STATUS_DISABLED = 0;
    const STATUS_ENABLED  = 1;

    /**
     * @return mixed
     * @throws \JustParallels\Exception
     */
    public function getCount()
    {
        $select = Bootstrap::getInstance()->getSql()
            ->select('stack')
            ->columns(array('count' => new Expression('MAX(id)')))
            ->where(array('status' => self::STATUS_ENABLED));


        $selectString = Bootstrap::getInstance()->getSql()->buildSqlString($select);
        $result       = Bootstrap::getInstance()->getDbAdapter()
            ->query($selectString, Adapter::QUERY_MODE_EXECUTE)
            ->current();

        return $result->count;
    }

    /**
     * @param $from
     * @param $to
     * @return array
     * @throws \JustParallels\Exception
     */
    public function getStacks($from, $to)
    {
        $where = new Where();
        $where->between('id', $from, $to);

        $select = Bootstrap::getInstance()->getSql()
            ->select('stack')
            ->where($where);

        $selectString = Bootstrap::getInstance()->getSql()->buildSqlString($select);
        $result       = Bootstrap::getInstance()->getDbAdapter()
            ->query($selectString, Adapter::QUERY_MODE_EXECUTE)
            ->toArray();

        return $result;
    }

    /**
     * @param $version
     * @param $stackId
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     * @throws \JustParallels\Exception
     */
    public function setVersion($version, $stackId)
    {
        $updatedAt = new \DateTime();
        $update    = new Update('stack');
        $update->set(array('version' => $version, 'updated_at' => $updatedAt->format('Y-m-d H:i:s')));
        $update->where(array('id' => $stackId));

        return Bootstrap::getInstance()->getSql()->prepareStatementForSqlObject($update)->execute();
    }
}