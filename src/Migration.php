<?php
/**
 * Created by PhpStorm.
 * User: vmelnychuk
 * Date: 9/25/16
 * Time: 10:38 PM
 */

namespace JustParallels;


use JustParallels\Log\Message;
use JustParallels\Log\MigrationInfo;

abstract class Migration
{
    abstract public function getVersion();

    abstract public function up(MigrationInfo $migrationInfo);

    abstract public function down(MigrationInfo $migrationInfo);

    abstract public function log(Message $message);

}