<?php
/**
 * Created by PhpStorm.
 * User: vmelnychuk
 * Date: 10/17/16
 * Time: 12:14 AM
 */

namespace JustParallels\Log;


class MigrationInfo
{
    private $stackId;
    private $stackVersion;
    private $migrationVersion;


    /**
     * MigrationInfo constructor.
     * @param $stackId
     * @param $stackVersion
     * @param $migrationVersion
     */
    public function __construct($stackId, $stackVersion, $migrationVersion)
    {
        $this->stackId          = $stackId;
        $this->stackVersion     = $stackVersion;
        $this->migrationVersion = $migrationVersion;
    }

    /**
     * @return mixed
     */
    public function getStackId()
    {
        return $this->stackId;
    }

    /**
     * @param mixed $stackId
     */
    public function setStackId($stackId)
    {
        $this->stackId = $stackId;
    }

    /**
     * @return mixed
     */
    public function getStackVersion()
    {
        return $this->stackVersion;
    }

    /**
     * @param mixed $stackVersion
     */
    public function setStackVersion($stackVersion)
    {
        $this->stackVersion = $stackVersion;
    }

    /**
     * @return mixed
     */
    public function getMigrationVersion()
    {
        return $this->migrationVersion;
    }

    /**
     * @param mixed $migrationVersion
     */
    public function setMigrationVersion($migrationVersion)
    {
        $this->migrationVersion = $migrationVersion;
    }

}