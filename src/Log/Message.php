<?php
/**
 * Created by PhpStorm.
 * User: vmelnychuk
 * Date: 10/9/16
 * Time: 4:59 PM
 */

namespace JustParallels\Log;


class Message
{

    const TYPE__INFO  = 1;
    const TYPE__ERROR = 2;

    const EVENT__MIGRATION_GENERAL = 10;
    const EVENT__MIGRATION_EXISTS  = 11;
    const EVENT__MIGRATION_START  = 12;
    const EVENT__MIGRATION_END  = 13;

    private $type;
    private $message;
    private $event;
    /**
     * @var MigrationInfo
     */
    private $migrationInfo;


    /**
     * Message constructor.
     * @param MigrationInfo $migrationInfo
     * @param int $type
     * @param string $message
     * @param int $event
     */
    public function __construct(MigrationInfo $migrationInfo, $type = self::TYPE__INFO, $message = '',
                                $event = self::EVENT__MIGRATION_GENERAL)
    {
        $this->type          = $type;
        $this->message       = $message;
        $this->event         = $event;
        $this->migrationInfo = $migrationInfo;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return int
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @param int $event
     */
    public function setEvent($event)
    {
        $this->event = $event;
    }

    /**
     * @return MigrationInfo
     */
    public function getMigrationInfo()
    {
        return $this->migrationInfo;
    }

    /**
     * @param MigrationInfo $migrationInfo
     */
    public function setMigrationInfo($migrationInfo)
    {
        $this->migrationInfo = $migrationInfo;
    }
}