<?php
/**
 * Created by PhpStorm.
 * User: vmelnychuk
 * Date: 10/1/16
 * Time: 5:27 PM
 */

namespace JustParallels\Tasks;


class TaskManager
{

    /**
     * @var Stack
     */
    private $stack;

    /**
     * Manager constructor.
     */
    public function __construct()
    {
        $this->stack = new Stack();
    }

    /**
     * @return Stack
     */
    public function getStack()
    {
        return $this->stack;
    }
}