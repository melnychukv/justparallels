<?php
/**
 * Created by PhpStorm.
 * User: vmelnychuk
 * Date: 10/1/16
 * Time: 2:45 AM
 */

namespace JustParallels\Tasks;

use JustParallels\Bootstrap;

class Stack
{

    const STATE_SUCCESS = 1;

    const STATE_FAILED = 2;

    /**
     * CreateWorkers constructor.
     */
    public function __construct()
    {
    }

    public function add($stackId, $status, $version, $name = '')
    {
        $dateTime = new \DateTime();
        $dateTime = $dateTime->format('Y-m-d H:i:s');
        $insert   = Bootstrap::getInstance()->getSql()->insert('stack')
            ->columns(array(
                'stack_id',
                'status',
                'version',
                'name',
                'state',
                'created_at',
                'updated_at'
            ))
            ->values(array(
                $stackId,
                (bool)$status,
                $version,
                $name,
                self::STATE_SUCCESS,
                $dateTime,
                $dateTime

            ));

        Bootstrap::getInstance()->getDbAdapter()
            ->query(Bootstrap::getInstance()->getSql()->buildSqlString($insert))
            ->execute();
    }

    public function disable($stackId)
    {

    }

    public function enable($stackId)
    {

    }

}