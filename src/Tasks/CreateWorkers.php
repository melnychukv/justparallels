<?php
/**
 * Created by PhpStorm.
 * User: vmelnychuk
 * Date: 10/1/16
 * Time: 2:45 AM
 */

namespace JustParallels\Tasks;


use JustParallels\Bootstrap;
use JustParallels\Model\Worker;

class CreateWorkers
{
    private $parallels;
    private $count;


    /**
     * CreateWorkers constructor.
     * @param $parallels
     */
    public function __construct($parallels)
    {
        $this->parallels = $parallels;
        $this->count     = Bootstrap::getInstance()->getModelsManager()->getStack()->getCount();
    }

    public function create($version)
    {
        $stacks = $this->getStacksInPart();

        for ($i = 0; $i < $this->parallels; $i++) {
            list($part, $from, $to) = $this->getParams($i, $stacks);
            $this->createPart($part, $from, $to, $version);
        }
    }

    protected function getStacksInPart()
    {
        return ceil($this->count / $this->parallels);
    }

    /**
     * @param $part
     * @param $from
     * @param $to
     * @param $version
     * @throws \JustParallels\Exception
     */
    protected function createPart($part, $from, $to, $version)
    {
        $dateTime = new \DateTime();
        $dateTime = $dateTime->format('Y-m-d H:i:s');

        $columns = array('state', 'part', 'from', 'to', 'version', 'created_at', 'updated_at');

        $values = array(Worker::STATE_STACK, $part, $from, $to, $version, $dateTime, $dateTime);
        Bootstrap::getInstance()->getModelsManager()->getWorker()->create($columns, $values);
    }

    /**
     * @param $i
     * @param $stacks
     * @return array
     */
    public function getParams($i, $stacks)
    {
        $part = $i + 1;
        $to   = $part * $stacks;
        $from = $to - $stacks;
        if ($to > $this->count) {
            $to = $this->count;
            return array($part, $from, $to);
        }
        return array($part, $from, $to);
    }

}