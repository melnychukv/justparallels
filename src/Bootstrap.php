<?php
/**
 * Created by PhpStorm.
 * User: vmelnychuk
 * Date: 9/30/16
 * Time: 11:07 PM
 */

namespace JustParallels;


use JustParallels\Model\ModelManager;
use JustParallels\Modules\DbAdapter;
use JustParallels\Tasks\TaskManager;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;

class Bootstrap
{

    /** @var Bootstrap */
    private static $_instance;

    /** @var Adapter */
    private $_dbAdapter;

    /** @var Configuration */
    private $_configuration;

    /**
     * @var TaskManager
     */
    private $_taskManager;

    private $_modelsManager;

    public function __construct()
    {

    }

    /**
     * @param Configuration $configuration
     */
    public static function init(Configuration $configuration)
    {
        self::$_instance = new self();
        $dbAdapter = new DbAdapter($configuration);
        self::$_instance->setDbAdapter($dbAdapter->build());
        self::$_instance->setConfiguration($configuration);
        self::$_instance->setTaskManager(new TaskManager());
        self::$_instance->setModelsManager(new ModelManager());
    }

    /**
     * @return Bootstrap
     * @throws Exception
     */
    public static function getInstance()
    {
        if (!self::$_instance) {
            throw new Exception('after working need call method init()');
        }
        return self::$_instance;
    }

    /**
     * @param Bootstrap $instance
     */
    public static function setInstance(Bootstrap $instance)
    {
        self::$_instance = $instance;
    }

    /**
     * @return Adapter
     */
    public function getDbAdapter()
    {
        return $this->_dbAdapter;
    }

    /**
     * @param Adapter $_dbAdapter
     */
    public function setDbAdapter($_dbAdapter)
    {
        $this->_dbAdapter = $_dbAdapter;
    }

    /**
     * @return Configuration
     */
    public function getConfiguration()
    {
        return $this->_configuration;
    }

    /**
     * @param Configuration $_configuration
     */
    public function setConfiguration($_configuration)
    {
        $this->_configuration = $_configuration;
    }

    /**
     * @return Sql
     */
    public function getSql()
    {
        return new Sql($this->getDbAdapter());
    }

    /**
     * @return TaskManager
     */
    public function getTaskManager()
    {
        return $this->_taskManager;
    }

    /**
     * @param TaskManager $_taskManager
     */
    public function setTaskManager($_taskManager)
    {
        $this->_taskManager = $_taskManager;
    }

    /**
     * @return ModelManager
     */
    public function getModelsManager()
    {
        return $this->_modelsManager;
    }

    /**
     * @param ModelManager $modelsManager
     */
    public function setModelsManager($modelsManager)
    {
        $this->_modelsManager = $modelsManager;
    }

}